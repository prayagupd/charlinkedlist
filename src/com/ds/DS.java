package com.ds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * 
 * uses java 8
 * @author prayagupd
 *
 */

interface ICharList {
	public String readString(final String listName, final String string);
	public String writeString(final String listName);
	public void clear(final String string);
	public int len(final String listName);
	public boolean equals(String s1, String s2);
	public String concatenate(String target, String s2);
	public String insert(String targetListName,String stringToInsert, final int startPosition);
	public int search(final String searchString, final String targetName, final int startPos);
	public String delete(final String listName,final int start,final int span);
	public String subString(final String targetListName,final String string, final int start, final int span);
}

class CharListImpl implements ICharList {
	
	List<String> A = new LinkedList<String>();
	List<String> B = new LinkedList<String>();
	List<String> C = new LinkedList<String>();
	
	final Map<String, List<String>> map = new HashMap<String, List<String>>(){
		private static final long serialVersionUID = 1L;

	{
		put("A", A);
		put("B", B);
		put("C", C);
	}};
	
	@Override
	public String readString(final String listName, final String string) {
		for(int index = 0 ; index < string.length() ; index++ ) {
			map.get(listName).add(""+string.charAt(index));
		}
		return string;
	}

	@Override
	public String writeString(final String listName) {
		Iterator<String> iterator = map.get(listName).iterator();
		String str = "";
		while (iterator.hasNext()) {
			str = str + iterator.next();
		}
		return str;
	}

	@Override
	public void clear(final String listName) {
		Iterator<String> iterator = map.get(listName).iterator();
		while(iterator.hasNext())
			iterator.remove();
	}

	@Override
	public int len(final String listName) {
		int size = 0;
		Iterator<String> iterator = map.get(listName).iterator();
		while(iterator.hasNext()){			
			size += 1;
			iterator.next();
		}
		return size;
	}

	@Override
	public boolean equals(String listName1, String listName2) {
		Iterator<String> iterator1 = map.get(listName1).iterator();
		Iterator<String> iterator2 = map.get(listName2).iterator();
		if(!iterator1.hasNext() || !iterator2.hasNext()){
			return false;
		}
		while (iterator1.hasNext() && iterator2.hasNext()) {
			if(iterator1.next()!=iterator2.next()) {
				return false;
			}
		}
		return true;
	}

	//uses java8 forEach feature
	@Override
	public String concatenate(String targetListName, String sourceListName) {
		List<String> target = map.get(targetListName);
		List<String> source = map.get(sourceListName);
		source.forEach(character -> { //same as taking .iterator()
			target.add(character);
		});
		return writeString(targetListName);
	}
	
	//uses java8 forEach feature
	@Override
	public String insert(String targetListName, String stringToInsert,
			int startPosition) {
		List<String> target = map.get(targetListName);
		List<String> newTarget = new LinkedList<String>();
		
		//insert upto position
		Iterator<String> targetIt = target.iterator();
		int positionIndex = 0;
		while(targetIt.hasNext()) {
			if(positionIndex >= startPosition-1) {
				break;
			}
			newTarget.add(targetIt.next());
			positionIndex++;
		}
		//System.out.println("1 " + newTarget.toString());
		
		//insert string
		char[] array = stringToInsert.toCharArray();
		List<String> list = new LinkedList<String>();
		for(char c : array){
			list.add(c+"");
		}
		list.forEach(character -> {
			newTarget.add(character+"");
		});
		//System.out.println("2 " + newTarget.toString());
		
		//add remaining of target
		IntStream.range(startPosition-1, target.size())
		  .forEach(positionIndex_ -> {
			newTarget.add(target.get(positionIndex_));
		  });
		

		target.clear();
		newTarget.forEach(character -> {
			target.add(character);
		});
		//System.out.println("3 " + target.toString());
		return writeString(targetListName);
	}


	@Override
	public int search(String targetName, String searchString, int startPos) {
		List<String> targetList = map.get(targetName);
		
		; int targetIndex = startPos-1;
		for(int searchIndex = 0; searchIndex < searchString.length() ; searchIndex++, targetIndex++ ) {
			final String compare  = searchString.charAt(searchIndex)+"";
			final String compareTo = targetList.get(targetIndex);
			if(!(compare).equals(compareTo)){
				return 0;
			}
		}
		return 1;
	}
	
	@Override
	public String delete(final String listName,final int startPosition, final int span) {
		List<String> targetList = map.get(listName);
		targetList.subList(startPosition-1, startPosition + span).clear();
		return writeString(listName);
	}
	
	@Override
	public String subString(final String targetListName,final String sourceListName, final int startPosition, final int span) {
		List<String> sourceList = map.get(sourceListName);
		List<String> targetListReference = map.get(targetListName);
		List<String> stringToAssign =  new LinkedList<String>();
		IntStream.range(startPosition-1, ((startPosition) + span))
		  .forEach(positionIndex -> {
			  stringToAssign.add(sourceList.get(positionIndex));
		  });
		
		// assign doesn't work
		//targetListReference = stringToAssign;
		targetListReference.clear();
		stringToAssign.forEach(character -> {
			targetListReference.add(character);
		});
		return writeString(targetListName);
	}
}


public class DS {
	public static void main(String[] args) throws Exception {
		System.out.println("enter command -> ");
		String action = getString();
		ICharList list = new CharListImpl();
		
		do {
			
			switch(action) {
				case "R" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					System.out.println("enter text -> ");
					final String text = getString();
					
					list.readString(stringName, text);
					list.readString("B", "Apple");
					break;
				}
				case "W" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final String textBack = list.writeString(stringName);
					System.out.println("Text => " + textBack);
					break;
				}				
				case "L" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final int len = list.len(stringName);
					System.out.println("len A => " + len);
					break;
				}
				case "C" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final String targetString = list.concatenate(stringName, "B");
					System.out.println("target string concatenate => " + targetString);
					break;
				}
				case "I" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final String insertedString = list.insert(stringName, "rayag", 2);
					System.out.println("inserted string => " + insertedString);
					break;
				}
				case "S" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final int searchString = list.search(stringName, "rayag", 2);
					System.out.println("search string => " + searchString);
					break;
				}
				case "E" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final boolean equals = list.equals(stringName, "B");
					System.out.println("equals B => " + equals);
					break;
				}				
				case "D" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final String deleteString = list.delete(stringName, 1, 3);
					System.out.println("delete string => " + deleteString);
					break;
				}
				case "T" : {
					System.out.println("enter string name -> ");
					final String stringName = getString();
					
					final String subString = list.subString(stringName, "B", 1, 3);
					System.out.println("sub string => " + subString);
					break;
				}
				
				default : {
					break;
				}
					
			}

			System.out.println("enter command -> ");
			action = getString();
			
		} while(!action.equals("QUIT"));
	}
	
	 //reads a string from the keyboard input
   public static String getString() throws IOException {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
   }

}